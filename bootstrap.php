<?php

namespace Litpi;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\SyslogHandler;
use Slim\App as App;
use Spiral\Middleware\ApiRateLimit;
use Spiral\Middleware\ExecutionLogger;
use Spiral\Middleware\JwtAuthentication;
use Spiral\Middleware\CorsMiddleware;
use Spiral\Middleware\RestApiLogger;
use Spiral\Middleware\RoleExtract;
use Spiral\Middleware\TrustedNetworkAccess;
use Spiral\PdoProxy;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Teamcrop\Rest\Base;

define('CORE_DIR', dirname(__FILE__) . DIRECTORY_SEPARATOR);
require BASE_DIR . '../Vendor/autoload.php';
require BASE_DIR . '../private/config.php';

require CORE_DIR . 'classmap.php';
require CORE_DIR . 'Middleware/RestApiLogger.php';
require CORE_DIR . 'Middleware/ExecutionLogger.php';
require CORE_DIR . 'Middleware/JwtAuthentication.php';
require CORE_DIR . 'Middleware/TrustedNetworkAccess.php';
require CORE_DIR . 'Middleware/CorsMiddleware.php';
require CORE_DIR . 'Middleware/RoleExtract.php';
require CORE_DIR . 'Middleware/ApiRateLimit.php';
require CORE_DIR . 'Spiral/RateLimit/RateLimit.php';
require CORE_DIR . 'Spiral/RateLimit/Adapter.php';
require CORE_DIR . 'Spiral/RateLimit/Adapter/Redis.php';
require CORE_DIR . 'Spiral/PdoProxy.php';
require CORE_DIR . 'BaseController.php';
require CORE_DIR . 'BaseModel.php';
require CORE_DIR . 'DebugConsole.php';


/** @var array $conf */

//Store access trusted key for used in sdk
if (isset($conf['trustednetworkaccess']) && isset($conf['trustednetworkaccess']['key'])) {
    Base::$accessTrustedKey = $conf['trustednetworkaccess']['key'];
}


//Setup baseurl for all sdk client request
if (isset($conf['sdk'])) {
    if (isset($conf['sdk']['baseurl']) && $conf['sdk']['baseurl'] != '') {
        Base::$baseurl = $conf['sdk']['baseurl'];
    }

    if (isset($conf['sdk']['debug']) && $conf['sdk']['debug'] == true) {
        Base::$debug = true;
    }
}


if (isset($conf['db']['tableprefix'])) {
    define('TABLE_PREFIX', $conf['db']['tableprefix']);
}

// Set host for using in another features related to URL concat
if (isset($conf['host'])) {
    define('HOST', $conf['host']);
} else {
    define('HOST', 'localhost');
}

//Set timezone from config
if (isset($conf['timezone'])) {
    date_default_timezone_set($conf['timezone']);
}

//Register autoload for Model, Controller autoload class
spl_autoload_register('autoloadtc');

// Setting for error reporting
if (isset($conf['phperror'])) {
    error_reporting(E_ALL & ~E_NOTICE);
    ini_set('display_errors', $conf['phperror']['display']);
}
//logger config for this service
$logger = new Logger(SERVICE_NAME);
$formatter = new LineFormatter("%message%\n");
$handler = new SyslogHandler(SERVICE_NAME, LOG_USER, Logger::DEBUG);
$handler->setFormatter($formatter);
$logger->pushHandler($handler);

// Init PDO Wrapper for MySQL
$db = new PdoProxy();
$db->addMaster($conf['db']['host'], $conf['db']['user'], $conf['db']['pass'], $conf['db']['name']);

//detect if has slave db config
if (isset($conf['db_slave']) && is_array($conf['db_slave'])) {
    $db->addSlave($conf['db_slave']['host'], $conf['db_slave']['user'], $conf['db_slave']['pass'], $conf['db_slave']['name']);
} else {
    $db->addSlave($conf['db']['host'], $conf['db']['user'], $conf['db']['pass'], $conf['db']['name']);
}


//INIT REGISTRY VARIABLE - MAIN STORAGE OF APPLICATION
$registry = Registry::getInstance();
$registry->set('db', $db);
$registry->set('conf', $conf);
$registry->set('logger', $logger);

$slimSettings =  [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

//Init slim app object
$app = new App($slimSettings);

//Add global server exception handling
$container = $app->getContainer();

$container['errorHandler'] = function ($container) {
    return function ($request, $response, $exception) use ($container) {
        return $container['response']
            ->withStatus(500)
            ->withHeader('Content-Type', 'text/html')
            ->write($exception->getMessage());
    };
};

$request = $container->get('request');

/////////////////////////
///// Important, process to include controller file
///This is the main different with LITPI framework core
//Parsing route information to include module/controller
$route = trim($request->getUri()->getPath(), '/');
$parts = explode('/', $route);
for ($i = 0; $i < count($parts); $i++) {
    $parts[$i] = htmlspecialchars($parts[$i]);
}
$module = (string)array_shift($parts);
$controller = (string)array_shift($parts);
$action = (string)array_shift($parts);
$registry->set('route', $route);
$registry->set('module', $module);
$registry->set('controller', $controller);
$registry->set('action', $action);
$class = '\\controller\\' . $module . '\\' . $controller;

//Generate Chain-Identifier
Base::$myChainIdentifier = $request->getUri()->getPath() . '-'.date('YmdHis') . '-'. substr(microtime(true), -4);

//check if valid controller
if ($action != 'ping' && classmap($class) != '') {

    //Init middleware (first add, last check ^^!)
    /** @var array $nonSecureList */
    $app->add(new RoleExtract());
    $app->add(new JwtAuthentication($nonSecureList, $conf['jwt']['publickey']));
    $app->add(new ExecutionLogger());
    $app->add(new ApiRateLimit());
    $app->add(new TrustedNetworkAccess());
    $app->add(new CorsMiddleware());


    /** @var \Controller\BaseController $myControllerObj */
    $myControllerObj = new $class($registry, $app);
    $myControllerObj->run();

} elseif ($action == 'ping') {

    //Init middleware (first add, last check ^^!)
    //Ping use to get  execution time
    $app->add(new ExecutionLogger());

    $app->get('/' . $module . '/' . $controller . '/ping', function(ServerRequestInterface $request, ResponseInterface $response){
        return $response->getBody()->write('PONG');
    });
}

$app->run();


//close db connections
$db->close();
