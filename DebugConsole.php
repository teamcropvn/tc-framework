<?php

namespace Litpi;

use Slim\App as App;

class DebugConsole
{
    public static $enable = false;
    public static $url = '';
    public static $service = '';
    public static $clientip = '';
    public static $app = null;

    public static function log($message)
    {
        $result = null;

        if (self::$enable) {
            if (!extension_loaded('curl')) {
                throw new \Exception('The curl extension is needed to use the DebugConsole.');
            }

            //Get request from app object
            /** @var App $app */
            $myRequest = self::$app->request;
            $chainIdentifier = '';
            if ($myRequest->hasHeader('Chain-Identifier')) {
                $chainIdentifier = $myRequest->getHeader('Chain-Identifier')[0];
            }

            $uriObj = $myRequest->getUri();
            $data = array(
                'clientip' => self::$clientip,
                'service' => self::$service,
                'method' => $myRequest->getMethod(),
                'scheme' => $uriObj->getScheme(),
                'host' => $uriObj->getHost(),
                'path' => $uriObj->getPath(),
                'querystring' => $uriObj->getQuery(),
                'chainidentifier' => $chainIdentifier,
                'data' => $message
            );
            $dataString = json_encode($data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, self::$url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($dataString))
            );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);

            //close connection
            curl_close($ch);
        }

        return $result;
    }
}
