<?php

namespace Spiral\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Litpi\Helper;
use Litpi\Registry;
use Spiral\RateLimit\Adapter\Redis;
use Spiral\RateLimit\RateLimit;

class ApiRateLimit
{
    /**
     * Rate Limit layer. Prevent spam DOS for system
     *
     * @param  ServerRequestInterface $request  PSR7 request
     * @param  ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        global $conf;

        $currentRequestIp = Helper::getIpAddress();
        $method = strtolower($request->getMethod());

        $registry = Registry::getInstance();
        $controller = $registry->get('controller');
        $action = $registry->get('action');
        $identifier = $currentRequestIp . '-' . $method . '-' . $controller ;

        $maxRequest = $this->getMaxRequest($method, $controller);
        $remain = $maxRequest;

        $pass = false;

        //If this is accesstrusted
        if ($registry->get('accesstrusted') === true
            || $this->isWhiteListIp($currentRequestIp)
            || $this->isWhiteListPath($method, $controller, $action)) {
            $pass = true;

        } else {
            try {
                $adapter = new Redis($conf['redis'][0]['ip'], $conf['redis'][0]['port']);
                $rateLimit = new RateLimit('tc-ratelitmit', $maxRequest, 60, $adapter);

                //start to check for pass or not
                if ($rateLimit->check($identifier) > 0) {
                    $pass = true;
                    $remain = $rateLimit->getAllow($identifier);
                }
            } catch (\Exception $e) {
                //In case has exception (maybe redis can not connect, we still mark this as pass)
                $pass = true;
            }
        }


        if ($pass) {
            // Get reference to application
            $response = $response->withHeader('X-Rate-Limit-Limit', $maxRequest)
                ->withHeader('X-Rate-Limit-Remaining', $remain)
                ->withHeader('X-Rate-Limit-Identifier', $identifier);

            $response = $next($request, $response);
        } else {
            /** @var ResponseInterface $response */
            $response = $response->withHeader('X-Rate-Limit-Limit', $maxRequest)
                ->withHeader('X-Rate-Limit-Remaining', 0)
                ->withHeader('X-Rate-Limit-Identifier', $identifier);

            $response = $response->withStatus(429);
            $response = $response->withHeader('Content-Type', 'application/json');
            $response->getBody()->write(json_encode(array('error_rate_limit_exceed')));
        }


        return $response;

    }

    /**
     * @param $method
     * @return int
     */
    private function getMaxRequest($method)
    {
        $method = strtolower($method);

        switch ($method) {
            case 'get':
                $maxRequest = 1000;
                break;
            case 'post':
                $maxRequest = 200;
                break;
            case 'put':
                $maxRequest = 200;
                break;
            case 'delete':
                $maxRequest = 200;
                break;
            default:
                $maxRequest = 500;
        }

        return $maxRequest;
    }

    /**
     * In some case, we need to whitelist some ip address of partner
     *
     * @param $ip
     * @return bool
     */
    private function isWhiteListIp($ip)
    {
        $whitelist = array(
            '123.30.104.78', // spiral beta
        );

        return in_array($ip, $whitelist);
    }

    /**
     * In some case, we need to whitelist some path (such as facebook webhook)
     *
     * @param $method
     * @param $controller
     * @param $action
     * @return bool
     */
    private function isWhiteListPath($method, $controller, $action)
    {
        $pass = false;

        $method = strtoupper($method);

        //define list of whitelist groups
        $whitelist = array(
            'facebookwebhook' => array('POST', 'ecomfacebook', 'webhook'),
            'regionlist' => array('GET', 'regions', ''),
        );

        foreach ($whitelist as $info) {
            if ($method == $info[0] && $controller == $info[1] && $action == $info[2]) {
                $pass = true;
            }
        }

        return $pass;
    }
}