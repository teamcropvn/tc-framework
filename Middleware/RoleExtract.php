<?php

namespace Spiral\Middleware;

use Litpi\Registry;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Teamcrop\Rest\RbacRole;
use Teamcrop\Utility\RbacSubjectItem;

class RoleExtract
{
    private $cache = true;

    public function __construct($cache = true)
    {
        $this->cache = $cache;
    }


    /**
     * Call to log request
     *
     * @param  ServerRequestInterface $request  PSR7 request
     * @param  ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        $registry = Registry::getInstance();

        //do not fetch role in access trusted request
        if ($registry->get('accesstrusted') === true) {
            $registry->set('role', array());
            $response = $next($request, $response);

        } elseif ($registry->get('company')->id == 0 || $registry->get('me')->id == 0) {
            //In case non-secure pass, do not extract role for this request
            $registry->set('role', array());
            $response = $next($request, $response);

        } else {
            //Request to get Role of current user
            $roleString = RbacRole::getDetailRole(
                $registry->get('company')->id,
                $registry->get('me')->id,
                $this->cache,
                $error
            );

            if (empty($error)) {
                $registry->set('role', $this->extractRole($roleString));
                $response = $next($request, $response);

            } else {
                /** @var ResponseInterface $response */
                $response = $response->withStatus(401)->withHeader('Content-type', 'application/json');
                $response->getBody()->write(json_encode(array('error' => $error)));
            }
        }


        return $response;
    }


    /**
     * Extract JWT role id list (in string format) to role detail list
     * @param $data
     * @return array
     */
    private function extractRole($data)
    {
        $role = array();
        if ($data != '') {
            $subjectInfoList = explode(',', $data);
            $subjectMapping = RbacSubjectItem::subjectMapping();
            foreach ($subjectInfoList as $subjectinfo) {
                //separate subjectid & objectid
                $parts = explode(':', $subjectinfo);
                $subjectid = $parts[0];
                $objectlist = array();
                if (count($parts) == 2) {
                    $objectlist = explode('-', $parts[1]);
                }
                if (isset($subjectMapping[$subjectid])) {
                    $ticket = $subjectMapping[$subjectid][0];

                    if (count($objectlist) > 0) {
                        $role[$ticket] = $objectlist;
                    } else {
                        $role[$ticket] = array();
                    }
                }
            }
        }

        return $role;
    }
}