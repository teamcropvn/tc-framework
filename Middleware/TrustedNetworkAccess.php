<?php

namespace Spiral\Middleware;

use Litpi\Registry;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class TrustedNetworkAccess
{
    /**
     * Call to set registry accesstrusted
     *
     * @param  ServerRequestInterface $request  PSR7 request
     * @param  ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        $trustedkey = $request->getHeader('AccessTrustedKey')[0];

        // Check whether need to check AccessTrusted
        if ($trustedkey != '') {
            $registry = Registry::getInstance();
            $conf = $registry->get('conf');

            //Check if requestor keys defined in allowkeys in config
            if (is_array($conf['trustednetworkaccess']['allowkeys'])
                && in_array($trustedkey, $conf['trustednetworkaccess']['allowkeys'])) {
                $registry->set('accesstrusted', true);
            }
        }

        $response = $next($request, $response);

        return $response;
    }
}