<?php

namespace Spiral\Middleware;

use Litpi\Helper;
use Litpi\Registry;
use Controller\BaseController;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Monolog\Logger;
use Teamcrop\Rest\Base;
use Teamcrop\Rest\LogRestRequest;

class ExecutionLogger
{
    /**
     * Call to log request
     *
     * @param  ServerRequestInterface $request  PSR7 request
     * @param  ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        /** @var ResponseInterface $response */
        $response = $next($request, $response);


        //Get execution time
        $executionTime = round(microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"], 3) * 1000;
        $response = $response->withHeader('Execution-Time', $executionTime . ' ms');
        if ($executionTime > 3000) {

            $registry = Registry::getInstance();
            $uritext = (string)$request->getUri();
            $uritext = preg_replace('/__jwtAuthorization=[0-9a-z.\-]+/ims', '{JWT}', $uritext);

            $slowLogContent = date('d/m/Y H:i:s - ');
            $slowLogContent .= $registry->get('accesstrusted') === true ? 'TRUSTED' : 'PUBLIC';
            $slowLogContent .= ' - ' . Helper::getIpAddress();
            $slowLogContent .= ' - ' . $request->getMethod() . ' ' . $uritext;
            $slowLogContent .= ' - Exec Time: ' . round($executionTime / 1000, 1) . 's ';
            $slowLogContent .= ' - STATUS ' . $response->getStatusCode();
            $slowLogContent .= PHP_EOL;

            $fh = @fopen('./uploads/slowrequest.txt', 'at');
            @fwrite($fh, $slowLogContent);
            if ($fh) {
                fclose($fh);
            }
        }

        //Set server addr to debug which server response to this request incase many server of same backend
        if (isset($_SERVER['SERVER_ADDR'])) {
            $response = $response->withHeader('Execution-Server', $_SERVER['SERVER_ADDR']);
        }

        // Get usaged memory (not allocated) until this point
        // In Megabype (MB) unit
        $memoryUsageInMegabyte = round(memory_get_usage() / 1024 / 1024, 4);
        $response = $response->withHeader('Execution-Memory', $memoryUsageInMegabyte . ' mb');

        return $response;
    }
}