<?php

namespace Spiral\Middleware;

use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Litpi\Registry;
use Litpi\Cacher;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Teamcrop\Rest\Base;
use Teamcrop\Rest\Company;

class JwtAuthentication
{
    private $nonSecureRoutes = array();
    private $secret = '';

    public function __construct($nonSecureRoutes = array(), $secret = '')
    {
        $this->nonSecureRoutes = $nonSecureRoutes;
        $this->secret = $secret;
    }


    /**
     * Call to log request
     *
     * @param  ServerRequestInterface $request  PSR7 request
     * @param  ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        $registry = Registry::getInstance();

        $httpUnauthorizedStatus = 401;

        //Init current info
        $error = array();
        $accesszone = '';
        $accesstrusted = false;
        $myUser = null;
        $myCompany = null;
        $myEmployee = null;
        $planstore = 0;
        $authenticationPass = false;
        $jwt = '';

        $queryData = $request->getQueryParams();

        //get jwt from header
        if ($request->hasHeader('Authorization') && $request->getHeader('Authorization')[0] != '') {
            $jwt = $request->getHeader('Authorization')[0];

        } elseif (array_key_exists('__jwtAuthorization', $queryData) && $queryData['__jwtAuthorization'] != '') {
            $jwt = $queryData['__jwtAuthorization'];

        } else {
            //Fallback the Authorization from POST (__jwtAuthorization)
            $formData = $request->getParsedBody();
            if (isset($formData['__jwtAuthorization']) && $formData['__jwtAuthorization'] != '') {
                $jwt = $formData['__jwtAuthorization'];
            }
        }

        //Set authorization for sdk request
        Base::$authorizationToken = $jwt;

        //detect with nonSecure routes
        $needAuthentication = true;
        if (!empty($this->nonSecureRoutes)) {
            foreach ($this->nonSecureRoutes as $route => $methods) {

                //check route first
                if (strpos($registry->get('route'), $route) !== false) {
                    //found current route in nonsecure setting, now, check method
                    //If methods array is empty, it's mean it allow all method with this route will be unsecure
                    if (empty($methods) || in_array(strtoupper($request->getMethod()), $methods)) {
                        $needAuthentication = false;
                    }
                }
            }
        }

        //Extract JWT info
        if ($jwt != '') {

            //this will let you move the time window X seconds
            JWT::$leeway = 300;

            try {
                //verify token signature & decode json payload
                $tokenData = JWT::decode($jwt, $this->secret, array('HS256'));

                //check is not blackedlist
                if ($this->checkBlacklistToken(md5($jwt))) {
                    $error[] = 'error_jwt_blacklisted';
                    $httpUnauthorizedStatus = 400;

                } else {

                    //get allowzone for check zone permission
                    $accesszone = $tokenData->data->accesszone;

                    //Extract User Information
                    $myUser = new \Stdclass();
                    $myUser->id = $tokenData->data->user->id;
                    $myUser->fullname = empty($tokenData->data->user->fullname)
                        ? 'User #' . $myUser->id : $tokenData->data->user->fullname;

                    //Extract Company Info
                    $myCompany = new Company($tokenData->data->company->id, true);
                    $myCompany->id = $tokenData->data->company->id;
                    $planstore = $tokenData->data->company->planstore;


                    //Extract Employee Information
                    $myEmployee = new \Stdclass();
                    $myEmployee->id = $tokenData->data->employee->id;
                    $myEmployee->status = $tokenData->data->employee->status;


                    ///////////////////////////////
                    // IF CURRENT LOGGED USER IN ADMIN
                    // DEFAULT HE IS FROM TRUSTED NETWORK
                    if ($accesszone == 'admin') {
                        $accesstrusted = true;
                    }

                    //////////////////////////////////////////
                    // VERY IMPORTANT SECURITY CHECK
                    // ONLY ADMINISTRATOR CAN ACCESS ALL SECTION
                    // AND IF COMPANY ZONE MUST GO WITH COMPANY ID > 0
                    if ($accesszone == 'admin' || ($accesszone == 'company' && $myCompany->id > 0)) {
                        $authenticationPass = true;
                    } else {
                        $error[] = 'error_zone_invalid';
                    }

                }
            } catch(ExpiredException $e) {
                $error[] = 'error_jwt_expired';
                $httpUnauthorizedStatus = 400;

            } catch (\Exception $e) {
                $error[] = 'JWT Token error: ' . $e->getMessage();
            }
        }

        //authenpass when accesstrusted detect
        if ($registry->get('accesstrusted') === true) {
            $authenticationPass = true;
            $accesstrusted = true;
        }

        //final test to go next for stop and return 401 error
        if (empty($error) && (!$needAuthentication || ($needAuthentication && $authenticationPass))) {

            //Assign data
            $registry->set('accesszone', $accesszone);
            $registry->set('accesstrusted', $accesstrusted);
            $registry->set('me', $myUser);
            $registry->set('company', $myCompany);
            $registry->set('employee', $myEmployee);
            $registry->set('planstore', $planstore);

            //continue to execution
            $response = $next($request, $response);

        } else {

            //set default error for 401 status authen
            if (empty($error)) {
                $error[] = 'error_token_empty';
            }

            /** @var ResponseInterface $response */
            $response = $response->withStatus($httpUnauthorizedStatus)->withHeader('Content-type', 'application/json');
            $response->getBody()->write(json_encode(array('error' => $error)));
        }

        return $response;
    }


    /**
     * Check this jwt hash is blacklisted or not
     * @param $jwthash
     * @return bool
     */
    private function checkBlacklistToken($jwthash)
    {
        $isBlacklisted = false;

        //check is empty token
        if ($jwthash == '') {
            $isBlacklisted = true;
        } else {

            //Call to Redis (or something else) to check this tokenid is blacklisted to prevent MITM attach
            $myCacher = new Cacher(self::cacheKeyForBlacklist($jwthash));
            $row = $myCacher->get();

            //found cached and value is greater than zero (had been blackedlist)
            if ($row && $row > 0) {
                $isBlacklisted = true;
            }
        }


        return $isBlacklisted;
    }

    /**
     * Ham tra ve key de cache vao blacklist
     *
     * IMPORTANT: THIS FUNCTION MUST RETURN KEY FORMAT SAME AS MODELS USERSESSION (IN TC-USER SERVICE)
     *
     * @param string $jwthash
     * @return string
     */
    public static function cacheKeyForBlacklist($jwthash)
    {
        return 'tc_session_blacklist_' . $jwthash;
    }
}