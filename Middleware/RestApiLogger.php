<?php

namespace Spiral\Middleware;

use Litpi\Helper;
use Litpi\Registry;
use Controller\BaseController;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Monolog\Logger;
use Teamcrop\Rest\Base;
use Teamcrop\Rest\LogRestRequest;

class RestApiLogger
{
    /**
     * Call to log request
     *
     * @param  ServerRequestInterface $request  PSR7 request
     * @param  ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        //Get chain identifier from headers
        if ($request->hasHeader('Chain-Identifier')) {
            Base::$requestChainIdentifier = $request->getHeaderLine('Chain-Identifier');
        }

        //Continue with execution

        /** @var ResponseInterface $response */
        $response = $next($request, $response);

        // Get usaged memory (not allocated) until this point
        // In Megabype (MB) unit
        $memoryUsageInMegabyte = round(memory_get_usage() / 1024 / 1024, 4);

        // After response
        $registry = Registry::getInstance();

        $controller = $registry->get('controller');
        $action = $registry->get('action');

        //Get execution time
        $executionTime = round(microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"], 3) * 1000;

        //do not tracking options request
        $isAccessTrusted = $registry->get('accesstrusted') === true;

        //to prevent loops, we need to exclude logrestrequests controller
        //and do not track request from admin
        if ($request->getMethod() != 'OPTIONS' && $controller != 'logrestrequests' && strpos($controller, 'admin') !== 0) {

            $isAjax = false;
            if ($request->hasHeader('X-Requested-With')
                && $request->getHeader('X-Requested-With')[0] == 'XMLHttpRequest') {
                $isAjax = true;
            }

            //calculate request source type
            $requestSource = LogRestRequest::SOURCE_DIRECT;
            if ($isAjax) {
                $requestSource = LogRestRequest::SOURCE_AJAX;
            } else {
                if ($isAccessTrusted) {
                    $requestSource = LogRestRequest::SOURCE_INTERNAL;
                }
            }

            $companyId = (int)$registry->get('company')->id;
            $creatorId = (int)$registry->get('me')->id;

            $logData = array(
                'method' => 'POST',
                'url' => LogRestRequest::$serviceurl,
                'body' => array(
                    'company_id' => $companyId,
                    'creator_id' => $creatorId,
                    'service' => SERVICE_NAME,
                    'method' => $request->getMethod(),
                    'controller' => $controller,
                    'action' => $action ? $action : 'index',
                    'uri' => (string)$request->getUri(),
                    'source' => $requestSource,
                    'status_code' => $response->getStatusCode(),
                    'input' => (string)($request->getBody()),
                    'output' =>  substr($response->getBody()->getContents(), 0, 512),
                    'exec_time' => $executionTime,
                    'memory' => $memoryUsageInMegabyte,
                    'chain_identifier' => Base::$requestChainIdentifier,
                    'ip_address' => Helper::getIpAddress(),
                    'ip_address_host' => $_SERVER['SERVER_ADDR'],
                    'admin_note' => '',
                    'severity' => LogRestRequest::SEVERITY_LOW,
                    'date_created' => time()
                )
            );
            BaseController::queueWebhookStatic($logData);

//
//            $formData = array(
//                'a' => $request->getMethod(),
//                'b' => $controller,
//                'c' => $action ? $action : 'index',
//                'd' => (int)$isAjax,
//                'e' => (int)$registry->get('company')->id,
//                'f' => (int)$registry->get('me')->id,
//                'g' => $response->getStatusCode(),
//                'h' => $executionTime,
//                'i' => $memoryUsageInMegabyte,
//                'j' => Helper::getIpAddress(),
//                'k' => substr_count(Base::$requestChainIdentifier, ',')
//            );
//
//
//            /** @var Logger $logger */
//            $logger = $registry->get('logger');
//
//            // Insert Log information
//            $output = '';
//            foreach ($formData as $k => $v) {
//                $output .= $k . '=' . $v . ',';
//            }
//
//            $logger->addNotice($output);
        }

        //Set server addr to debug which server response to this request incase many server of same backend
        if ($action == 'ping' && isset($_SERVER['SERVER_ADDR'])) {
            $response = $response->withHeader('Execution-Server', $_SERVER['SERVER_ADDR']);
        }

        $response = $response->withHeader('Execution-Time', $executionTime . ' ms');
        return $response;
    }
}