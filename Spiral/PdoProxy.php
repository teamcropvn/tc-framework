<?php

namespace Spiral;

use \Litpi\MyPdoProxy as MyPdoProxy;

class PdoProxy extends MyPdoProxy
{
    /**
     * In some case, we need to force request to MASTER db for doing on latest inserted data (such as post-processing..)
     * @var bool
     */
    public static $forceMaster = false;

    /**
     * Base on the SQL query, we will route the query to valid/suitable
     * replicate(master or slave) to load balancing between db servers
     *
     * @param $sql
     * @param string $hosttype
     * @param string $host
     * @param string $querytype
     * @param string $table
     * @return mixed
     */
    protected function getReplicateDb($sql, &$hosttype = '', &$host = '', &$querytype = '', &$table = '')
    {
        $sql = trim($sql);

        if (stripos($sql, 'SELECT') !== 0) {
            //Not select query
            if (preg_match('/insert\s+into\s+([a-z0-9_]+)/ims', $sql, $match)) {
                $table = $match[1];
                $querytype = 'INSERT';
            } elseif (preg_match('/update\s+([a-z0-9_]+)/ims', $sql, $match)) {
                $table = $match[1];
                $querytype = 'UPDATE';
            } elseif (preg_match('/delete\s+from\s+([a-z0-9_]+)/ims', $sql, $match)) {
                $table = $match[1];
                $querytype = 'DELETE';
            } else {
                $querytype = 'OTHER';
            }
        } else {
            $querytype = 'SELECT';

            if (preg_match('/^select.*?from\s+([a-z0-9_]+)/ims', $sql, $match)) {
                $table = $match[1];
            }
        }

        //////////////////////
        // SELECT DB BASE ON QUERY TYPE
        if ($querytype == 'SELECT' && !self::$forceMaster) {
            $replicateCount = count($this->connectinfo['slave']);

            //select slave
            if ($replicateCount == 0) {
                die('slave DB Config not found.');
            } else {
                //Select a random slave info
                $randomConnectInfo = array();
                $randseed = rand(1, $replicateCount);
                $i = 1;
                foreach ($this->connectinfo['slave'] as $connectinfo) {
                    if ($i == $randseed) {
                        $randomConnectInfo = $connectinfo;
                        $hosttype = 'slave';
                        $host = $connectinfo['host'];
                    }
                    $i++;
                }

                /////////////////////////////////
                $db = $this->initConnection(false, $randomConnectInfo['identifier']);

            }
        } else {
            $replicateCount = count($this->connectinfo['master']);
            if ($replicateCount == 0) {
                die('Master DB Config not found.');
            } else {
                //Select a random master info
                $randomConnectInfo = array();
                $randseed = rand(1, $replicateCount);
                $i = 1;
                foreach ($this->connectinfo['master'] as $connectinfo) {
                    if ($i == $randseed) {
                        $randomConnectInfo = $connectinfo;
                        $hosttype = 'master';
                        $host = $connectinfo['host'];
                    }
                    $i++;
                }

                /////////////////////////////////
                $db = $this->initConnection(true, $randomConnectInfo['identifier']);

                if (self::$forceMaster) {
//                    $fh = fopen('./uploads/forcemaster.txt', 'at');
//                    fwrite($fh, $sql . PHP_EOL . PHP_EOL);
//                    fclose($fh);
                }
            }
        }

        //////////////
        return $db;
    }

    /**
     * Override parent query
     * @param $sql
     * @param array $params
     * @return mixed
     */
    public function query($sql, $params = array())
    {
        global $logger, $registry;

        $this->latestReplicate = $replicatedb = $this->getReplicateDb($sql, $hosttype, $host, $querytype, $table);

        $myTimer = new \Litpi\Timer;
        $myTimer->start();
        $stmt = $replicatedb->query($sql, $params);
        $myTimer->stop();
        $execQueryTime = $myTimer->getExecTime();

        //tracking
        if (!is_null($logger)) {
            $formData = array(
                'a' => $hosttype,
                'b' => $host,
                'c' => $querytype,
                'd' => $table,
                'e' => $execQueryTime,
                'f' => $registry->get('company')->id,
                'g' => $registry->get('me')->id
            );

            // Insert Log information
            $output = '';
            foreach ($formData as $k => $v) {
                $output .= $k . '=' . $v . ',';
            }

            $logger->addNotice('pdoproxy => ' . $output);
        }

        return $stmt;
    }

    /**
     * Closed all connections to masters and slaves server
     */
    public function close()
    {
        foreach ($this->master as $pdoObject) {
            $pdoObject = null;
        }

        foreach ($this->slave as $pdoObject) {
            $pdoObject = null;
        }
    }
}
